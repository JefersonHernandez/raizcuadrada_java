import java.util.Scanner;

/**
 * autor Jeferson Hernandez
 */
public class RaizCuadrada {

	public RaizCuadrada() {
	}

	/**
	 * Dado un numero entero devuelve la raiz exacta si es un numero cuadratico. de
	 * otra manera devuelve su raiz aproximada.
	 */
	public static float calcularRaiz(int numero) {
		int i = 0;
		double aux = 0;
		while ((aux = (i * i)) <= numero) {
			if (aux == numero) {
				return i;
			}
			i++;
		}
		i--;

		double decimal = 0.1;
		while ((aux = getCuadrado(i + decimal)) < numero) {
			decimal += 0.1;
		}

		return (float) (i + decimal - 0.1);
	}

	/**
	 * Devuelve el cuadrado de un numero ejemplo
	 * dado un numero  = 16 retorna 4.0
	 * @param numero
	 * @return
	 */
	private static double getCuadrado(double numero) {
		return numero * numero;
	}

	public static void main(String[] str) {
		Scanner leer = new Scanner(System.in);
		System.out.print("ingresar numero: ");
		int numero = leer.nextInt();
		System.out.print("raiz cuadrada de : " + numero + " es --> " + calcularRaiz(numero));
	}
}